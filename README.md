

#### 介绍
基于bytebuddy实现，用于探测系统性能瓶颈，具体耗时链路，能探测到Java方法级别，简单易用.
- 作者微信：lakernote
- 公众号：Java大厂面试官

#### 使用说明
在需要监控的应用前面加上如下命令
```bash
-javaagent:D:\soft\agent.jar=com.laker.monitor
```
>后面的是需要追踪的包路径

#### 效果
```bash
`---[main]-laker.trace-[start]---
    `---[706.6179ms] [main] com.laker.monitor.test.AgentTest.methodAll
        +---[max]:[404.2633ms] [main] com.laker.monitor.test.AgentTest.method1
        |   +---[201.4098ms] [main] com.laker.monitor.test.AgentTest.method1_1
        |   |   `---[100.6594ms] [main] com.laker.monitor.test.AgentTest.method1_1_1
        |   `---[100.6184ms] [main] com.laker.monitor.test.AgentTest.method1_2
        +---[100.6495ms] [main] com.laker.monitor.test.AgentTest.method2
        +---[100.6395ms] [main] com.laker.monitor.test.AgentTest.method3
        `---[100.7ms] [main] com.laker.monitor.test.AgentTest.method4
`---[main]-laker.trace-[end]---
```