package com.laker.monitor.agent.bytebuddy.listener;

import net.bytebuddy.agent.builder.AgentBuilder;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.utility.JavaModule;

/**
 * @author laker
 */
public class LogListener {
    public static final AgentBuilder.Listener listener = new AgentBuilder.Listener() {
        /**
         *在应用成功的转换之前调用。
         *
         *@param typeDescription 正在转换的类型。
         *@param classLoader 正在加载此类型的类加载器。
         *@param module 转换类型的模块，如果当前VM不支持模块，则为{@code null}。
         *@param dynamicType 创建的动态类型。

         */
        @Override
        public void onTransformation(TypeDescription typeDescription, ClassLoader classLoader, JavaModule module, DynamicType dynamicType) {
        }

        /**
         *当类型未转换但被忽略时调用。
         *
         *@param typeDescription 转换时忽略的类型。
         *@param classLoader 正在加载此类型的类加载器。
         *@param module 被忽略类型的模块，如果当前VM不支持模块，则为{@code null}。

         */
        @Override
        public void onIgnored(TypeDescription typeDescription, ClassLoader classLoader, JavaModule module) {
        }

        @Override
        public void onError(String typeName, ClassLoader classLoader, JavaModule module, Throwable throwable) {
            throwable.printStackTrace();
        }

        /**
         *在尝试加载类之后调用，与该类的处理无关。
         *
         *@param typeName 检测类型的二进制名称。
         *@param classLoader 正在加载此类型的类加载器。
         *@param module 检测类型的模块，如果当前VM不支持模块，则为{@code null}。

         */
        @Override
        public void onComplete(String typeName, ClassLoader classLoader, JavaModule module) {
        }
    };

}
