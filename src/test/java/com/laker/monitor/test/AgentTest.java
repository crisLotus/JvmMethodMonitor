package com.laker.monitor.test;

import java.util.concurrent.TimeUnit;

public class AgentTest {

    public static void main(String[] args) throws InterruptedException {

        new AgentTest().methodAll();

        while (true) {
        }
    }

    public void methodAll() throws InterruptedException {
        method1();
        method2();
        method3();
        method4();
    }

    public void method1() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(100);
        method1_1();
        method1_2();
    }

    public void method1_1() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(100);
        method1_1_1();
    }


    public void method1_1_1() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(100);
    }

    public void method1_2() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(100);
    }

    public void method2() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(100);
    }

    public void method3() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(100);
    }

    public void method4() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(100);
    }
}
